
const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./routes/taskRoutes.js')
const app = express();
const port= 4001;


app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/tasks', taskRoutes);

mongoose.connect(`mongodb+srv://gabrieldguillermo:admin123@zuitt-batch-197.w7cnjly.mongodb.net/S36?retryWrites=true&w=majority`,{
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let db = mongoose.connection
db.on('error', () => console.error('Connection Error!'));
db.on('open',() => console.log('Connected to MongoDB!'));



app.listen(port, () => console.log(`Server is running at port ${port}`))
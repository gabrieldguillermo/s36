const express = require ('express');

const router = express.Router()
const TaskController = require('../controllers/taskControllers.js');

router.get('/', (req, res) => {
	TaskController.getAllTasks().then((resultFromController) => res.send(resultFromController))
});

router.get('/:id', (req, res) => {
	TaskController.getTask(req.params.id).then((resultFromController) => res.send(resultFromController))
});

router.put('/:id/complete', (req, res) => {
	TaskController.statusComplete(req.params.id, req.body).then((resultFromController) => res.send(resultFromController));
} )




module.exports = router

const Task = require('../models/Task.js');

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
};


module.exports.getTask = (taskId) =>{
	return Task.findById(taskId).then((result,error) =>{
		if(error){
			console.log(error)
				return error
		} else {
			return result
		}
	})
};


module.exports.statusComplete = (taskId,reqBody) => {
	return Task.findById(taskId).then((result,error) =>{
		if(error){
			console.log(error)
				return error
		} else if(result != null && result.status == 'Completed'){
			return `Status is already been ${result.status}`
		}else{
			if(reqBody.status != null){
				result.status = reqBody.status

				return result.save().then((updatedstatus, error) =>{
					if(error){
						console.log(error);
						return false
					} else {
						return updatedstatus
					}
				})
			}else{
				return result
			}
			

				
		}
	})
}

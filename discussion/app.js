// Express server setup
const express = require('express');
const mongoose = require('mongoose');
// This allows us to use all the routes defined in the "taskRoute.js"
const taskRoutes = require('./routes/taskRoutes.js')
const app = express();


const port = 3001;


mongoose.connect(`mongodb+srv://gabrieldguillermo:admin123@zuitt-batch-197.w7cnjly.mongodb.net/S36?retryWrites=true&w=majority`,{
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let db = mongoose.connection
db.on('error', () => console.error('Connection Error!'));
db.on('open',() => console.log('Connected to MongoDB!'));

//middleware

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/tasks', taskRoutes)


app.listen(port, () => console.log(`Server is running at port ${port}`));